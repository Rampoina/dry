# Minimal build

if ($PWD/script/cmake_generic.sh $PWD/$1 -DDRY_TOOLS=0 -DDRY_SAMPLES=0) then
    cd $PWD/$1;
    make;
    cd $PWD;
fi
